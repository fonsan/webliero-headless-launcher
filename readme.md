
# Headless Launcher

# Installation

Make sure [NodeJS](https://nodejs.org) is installed.

Then run:

```
npm install -g https://gitlab.com/basro/webliero-headless-launcher.git
```

### Linux and MacOS
On linux and macos if you are installing with sudo you need to run with unsafe-perm:
```
sudo NPM_CONFIG_UNSAFE_PERM=true npm install -g https://gitlab.com/basro/webliero-headless-launcher.git
```
At the moment of writing this, the npm option `--unsafe-perm` is bugged for git packages, but `NPM_CONFIG_UNSAFE_PERM` environment variable works so we use that.

Alternatively use the safer approach: [configure npm to install without sudo](https://github.com/sindresorhus/guides/blob/master/npm-global-without-sudo.md).


# Usage

Installing will add the `wlhl` command to your path. You can use this command to run and control your webliero headless server.

## server
This subcommand launches a chrome instance that will host the rooms.
To interact with the server (launch rooms or stop rooms) you must communicate with it using other `wlhl` commands.

This process should run in the background, using screen or tmux is a simple way to achieve this.
Only a single instance of server should be running in your computer.

Run with:

```
wlhl server
```

If you are on a graphical environment you can use the `--show` option to show the browser window.

```
wlhl server --show
```

## launch

Use launch to create a new room, the command will then follow the room's events in real time so you must terminate it manually with `Ctrl+C`.

```
wlhl launch <scripts...> --token=<token> --id=<roomid>
```

When launching a room from a non graphical environment you must use a headless token which can be obtained here: https://www.webliero.com/headlesstoken

If the --id option is ommitted the room will have the id `default`.

## stop

Use stop to stop an already running room.
```
wlhl stop <id>
```

## run

Use run to execute scripts on an already running room.

```
wlhl run <id> <scripts...>
```

### ls

Use ls to list the currently running rooms.

```
wlhl ls
```

### follow

Use follow to watch the logs of a room in real time.

```
wlhl follow <id>
```

### stats

Use stats to follow the browser metrics of the room in real time.

```
wlhl stats <id>
```

