console.log("Running Server...");
const room = window.WLInit({
	token: window.WLTOKEN,
	roomName: "Dedicated Server",
	maxPlayers: 12,
	public: false,
	password: null
});
window.WLROOM = room;

room.onRoomLink = (link) => console.log(link);
room.onCaptcha = () => console.log("Invalid token");

const admins = new Set(["bVtUs3APcp1LP1uM37cQvhePDaJYdrwWs0s4TJlldUQ"]);

room.onPlayerJoin = (player) => {
	if ( admins.has(player.auth) ) {
		room.setPlayerAdmin(player.id, true);
	}
}