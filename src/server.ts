#!/usr/bin/env node

import * as ipc from 'node-ipc';
import {Socket} from "net";
import * as fs from 'fs';
import * as path from 'path';
import * as puppeteer from 'puppeteer';
import { LaunchMsg, StopMsg, FollowMsg, MetricsMsg, MetricsResultMsg, RunScriptMsg } from './messages';
import {EventEmitter} from 'events';

const args = [
	"--disable-background-networking",
	"--disable-background-timer-throttling",
	"--disable-client-side-phishing-detection",
	"--disable-extensions",
	"--disable-sync",
	"--disable-translate",
	//"--allow-running-insecure-content",
	"--no-sandbox",
	"--no-first-run",
	"--disable-features=WebRtcHideLocalIpsWithMdns"
]

const pages = new Map<string, RoomPage>();
class RoomPage extends EventEmitter {
	constructor(public page: puppeteer.Page) {
		super();

		page.on("console", async msg => {
			const args = await Promise.all(msg.args().map(arg => arg.executionContext().evaluate(arg => {
				if (arg instanceof Error) {
					return `${arg.name}: ${arg.message}`;
				}
				return arg;
			}, arg)));
			this.log(args.join(" "));
		});
	
		page.on("pageerror", (error) => {
			this.log(error.message);
		});
	
		page.on("error", (error) => {
			this.log(error.message);
		});
	}

	async setToken(token:string) {
		await this.page.evaluate((token) => (window as any).WLTOKEN = token, token);
	}

	async loadHeadless() {
		this.log("Loading headless...");
		await this.page.goto("https://www.webliero.com/headless");
		await this.page.evaluate( function() {
			if ( (window as any).WLInit != null ) {
				return Promise.resolve();
			}
			return new Promise((resolve, reject) => {
				(window as any).onWLLoaded = () => resolve();
			});
		});
		this.log("Loaded headless");
	}

	async runScriptPath(scriptPath:string) {
		this.log(`Loading script ${scriptPath}`);
		const scriptFileContents = await fs.promises.readFile(path.resolve(scriptPath), "utf8");
		await this.page.evaluate(`(async function() { try { ${scriptFileContents}
			} catch(e) {
				console.log(e);
				throw new Error("Failed to run script.");
			}
		})();`);
	}

	log(msg:string) {
		this.emit('log', msg);
	}
}

async function createRoomPage(browser: puppeteer.Browser, id:string) {
	if ( pages.get(id) != null ) throw Error(`Cannot create room page '${id}' because it already exists.`);
	const roomPage = new RoomPage(await browser.newPage());
	pages.set(id, roomPage);
	roomPage.on('log', (msg) => console.log(`"${id}": `, msg));
	return roomPage;
}

export async function startServer(show:boolean, chromeExecutablePath? : string) {
	console.log("Starting chromium...");
	const browser = await puppeteer.launch({headless: !show, args, executablePath: chromeExecutablePath});
	console.log("Chromium up");

	ipc.config.id = 'wlserver';
	ipc.config.silent = true;
	ipc.config.maxRetries = 0;

	ipc.serve(function() {
		const srv = ipc.server;

		function followPage(socket:Socket, roomPage:RoomPage) {
			function stop() {
				roomPage.off('log', connectionLog);
				socket.destroy();
			}
			function connectionLog(msg:string) {
				srv.emit(socket, 'message', msg);
			}
			roomPage.on('log', connectionLog);
			socket.on('close', stop);
			return stop;
		}

		srv.on('error', function(err) {
			console.log(err);
		});

		srv.on('launch', async function(data: LaunchMsg,socket:Socket) {
			const {id, scriptPath, token} = data;
			const scripts = typeof scriptPath == "string"? [scriptPath]: scriptPath;
			if ( pages.get(id) != null ) {
				srv.emit(socket, 'message', `"${id}" is already running, you must stop it first.`);
				socket.end();
				return;
			}
			srv.emit(socket, 'message', `Launching room id="${id}" token="${token}"`);

			try {
				const roomPage = await createRoomPage(browser, id);
				followPage(socket, roomPage);
				await roomPage.loadHeadless();
				await roomPage.setToken(token);
				for(let script of scripts) {
					await roomPage.runScriptPath(script);
				}
			}catch(e) {
				srv.emit(socket, 'message', e.toString());
			}
		});

		srv.on('run-script', async function(data: RunScriptMsg, socket: Socket) {
			const {id, scriptPaths} = data;
			const roomPage = pages.get(data.id);
			if ( roomPage == null ) {
				srv.emit(socket, 'message', `"${data.id}" doesn't exist`);
				socket.end();
				return;
			}

			const stop = followPage(socket, roomPage);
			try {
				for(let script of scriptPaths) {
					await roomPage.runScriptPath(script);
				}
			}catch(e) {
				console.log(e.toString());
			}
			finally {
				// Wait a bit to allow console message events produced by the script to arrive before closing the cli connection.
				await new Promise(resolve => setTimeout(resolve, 50));
				stop();
			}
		});

		srv.on(
			'ls',
			function(data,socket:Socket){
				console.log("ls");
				for( let key of pages.keys() ) {
					srv.emit(socket, 'message', key);
				}
				socket.end();
			}
		);

		srv.on('stop', async function(data: StopMsg, socket:Socket) {
			const roomPage = pages.get(data.id);
			if ( roomPage == null ) {
				srv.emit(socket, 'message', `"${data.id}" doesn't exist`);
				socket.end();
				return;
			}

			pages.delete(data.id);
			if ( roomPage.page != null ) {
				await roomPage.page.goto("about:blank");
				await roomPage.page.close();
			}
			srv.emit(socket, 'message', `"${data.id}" closed`);
			socket.end();
		});

		srv.on('follow', async function(data: FollowMsg, socket:Socket) {
			const roomPage = pages.get(data.id);
			if ( roomPage == null ) {
				srv.emit(socket, 'message', `"${data.id}" doesn't exist`);
				socket.end();
				return;
			}

			srv.emit(socket, 'message', `Following room id="${data.id}"`);

			followPage(socket, roomPage);
		});

		srv.on('metrics', async function(data: MetricsMsg, socket:Socket) {
			const roomPage = pages.get(data.id);
			if ( roomPage == null || roomPage.page == null ) {
				srv.emit(socket, 'message', `"${data.id}" doesn't exist`);
				socket.end();
				return;
			}

			const page = roomPage.page;
			let done = false;
			socket.on('close', () => done=true);

			async function sendMetrics() {
				if ( done ) return;
				const metrics = await page.metrics();
				srv.emit(socket, 'metrics', {
					Id: data.id,
					Timestamp: metrics.Timestamp,
					JSHeapTotalSize: metrics.JSHeapTotalSize,
					JSHeapUsedSize: metrics.JSHeapUsedSize,
					ScriptDuration: metrics.ScriptDuration,
					TaskDuration: metrics.TaskDuration
				} as MetricsResultMsg);
				setTimeout(sendMetrics, 1000);
			}
			sendMetrics();
		});
	});

	ipc.server.start();

	browser.on('disconnected', function() {
		ipc.server.stop();
	});
}
